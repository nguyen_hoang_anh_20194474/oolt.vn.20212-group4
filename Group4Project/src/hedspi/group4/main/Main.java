package hedspi.group4.main;

import javax.swing.JFrame;


public class Main {

public static void main(String[] args) {
		
	
	JFrame window = new JFrame("Diamond Hunter");
	GamePanel gamePanel= new GamePanel();
	window.add(gamePanel);
	
	window.setResizable(false);
	window.pack();
	
	window.setLocationRelativeTo(null);
	window.setVisible(true);
	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	gamePanel.startGameThread();	
	}

}


