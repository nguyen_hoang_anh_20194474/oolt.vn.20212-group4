// The GamePanel is the drawing canvas.
// It contains the game loop which
// keeps the game moving forward.
// This class is also the one that grabs key events.

package hedspi.group4.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

import hedspi.group4.entity.Player;
import hedspi.group4.manager.KeyHandler;
import hedspi.group4.tilemap.TileManager;

public class GamePanel extends JPanel implements Runnable, KeyListener {
	public static final int ORIGINAL_TILE_SIZE = 16;
	public static final int SCALE = 3;

	public final static int TILE_SIZE = ORIGINAL_TILE_SIZE * SCALE;
	public static final int MAX_SCREEN_COL = 16;
	public static final int MAX_SCREEN_ROW = 12;

	public final static int SCREEN_WIDTH = TILE_SIZE * MAX_SCREEN_COL;
	public final static int SCREEN_HEIGHT = TILE_SIZE * MAX_SCREEN_ROW;

	public static final int MAX_WORLD_COL = 40;
	public static final int MAX_WORLD_ROW = 35;

	public final static int WORLD_WIDTH = TILE_SIZE * MAX_WORLD_COL;
	public final static int WORLD_HEIGHT = TILE_SIZE * MAX_WORLD_ROW;

	final int FPS = 60;
	private Thread gameThread;

	int playerX = 100;
	int playerY = 100;
	int playerSpeed = 2;

	public Player player = new Player(this);
	TileManager tileM = new TileManager(this);
	public CollisionChecker cChecker = new CollisionChecker(this);

	public GamePanel() {
		this.setPreferredSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT));
		setFocusable(true);
		requestFocus();

	}

	public void startGameThread() {

		if (gameThread == null) {
			addKeyListener(this);
			gameThread = new Thread(this);
			gameThread.start();
		}
	}

	@Override
	public void run() {
		double drawInterval = 1000000000 / FPS;
		double delta = 0;
		long lastTime = System.nanoTime();
		long currentTime;
		while (gameThread != null) {
			currentTime = System.nanoTime();
			delta += (currentTime - lastTime) / drawInterval;
			lastTime = currentTime;

			if (delta >= 1) {
				update();
				repaint();
				delta--;
			}

		}

	}

	public void update() {
		player.update();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		tileM.draw(g2);
		player.draw(g2);
		g2.dispose();
	}

	@Override
	public void keyTyped(KeyEvent key) {
	}

	@Override
	public void keyPressed(KeyEvent key) {
		KeyHandler.keySet(key.getKeyCode(), true);
	}

	@Override
	public void keyReleased(KeyEvent key) {
		KeyHandler.keySet(key.getKeyCode(), false);
	}

}
