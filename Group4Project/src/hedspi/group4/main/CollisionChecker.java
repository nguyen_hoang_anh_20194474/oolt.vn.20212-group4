package hedspi.group4.main;

import java.util.Iterator;

import hedspi.group4.entity.Entity;
import hedspi.group4.tilemap.Tile;
import hedspi.group4.tilemap.TileManager;

public class CollisionChecker {
	GamePanel gp;

	public CollisionChecker(GamePanel gp) {
		this.gp = gp;
	}

	public void checkTile(Entity entity) {
		int entityLeftWorldX = entity.worldX + entity.solidArea.x;
		int entityRightWorldX = entity.worldX + entity.solidArea.x + entity.solidArea.width;
		int entityTopWorldY = entity.worldY + entity.solidArea.y;
		int entityBottomWorldY = entity.worldY + entity.solidArea.y + entity.solidArea.height;

		int entityLeftCol = entityLeftWorldX / gp.TILE_SIZE;
		int entityRightCol = entityRightWorldX / gp.TILE_SIZE;
		int entityTopRow = entityTopWorldY / gp.TILE_SIZE;
		int entityBottomRow = entityBottomWorldY / gp.TILE_SIZE;

		int tileNum1, tileNum2;

		switch (entity.direction) {
		case "up":
			entityTopRow = (entityTopWorldY - entity.speed) / gp.TILE_SIZE;

			tileNum1 = gp.tileM.map[entityTopRow][entityLeftCol];
			tileNum2 = gp.tileM.map[entityTopRow][entityRightCol];
			if (tileNum1 >= TileManager.numTilesAcross || tileNum2 >= TileManager.numTilesAcross) {
				entity.collisionOn = true;
			}
			break;
		case "down":
			entityBottomRow = (entityBottomWorldY + entity.speed) / gp.TILE_SIZE;
			tileNum1 = gp.tileM.map[entityBottomRow][entityLeftCol];
			tileNum2 = gp.tileM.map[entityBottomRow][entityRightCol];
	
			if (tileNum1 >= TileManager.numTilesAcross || tileNum2 >= TileManager.numTilesAcross) {
				entity.collisionOn = true;
			}
			break;
		case "left":
			entityLeftCol = (entityLeftWorldX - entity.speed) / gp.TILE_SIZE;
			tileNum1 = gp.tileM.map[entityTopRow][entityLeftCol];
			tileNum2 = gp.tileM.map[entityBottomRow][entityLeftCol];
			
			if (tileNum1 >= TileManager.numTilesAcross || tileNum2 >= TileManager.numTilesAcross) {
				entity.collisionOn = true;
			}
			break;
		case "right":
			entityRightCol = (entityRightWorldX + entity.speed) / gp.TILE_SIZE;
			tileNum1 = gp.tileM.map[entityTopRow][entityRightCol];
			tileNum2 = gp.tileM.map[entityBottomRow][entityRightCol];
			
			if (tileNum1 >= TileManager.numTilesAcross || tileNum2 >= TileManager.numTilesAcross) {
				entity.collisionOn = true;
			}
			break;
		}
	}
}
