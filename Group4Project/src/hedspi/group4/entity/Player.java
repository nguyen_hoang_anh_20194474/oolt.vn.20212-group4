package hedspi.group4.entity;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import hedspi.group4.main.GamePanel;
import hedspi.group4.manager.Content;
import hedspi.group4.manager.KeyHandler;

public class Player extends Entity {

	private BufferedImage[] downSprites;
	private BufferedImage[] leftSprites;
	private BufferedImage[] rightSprites;
	private BufferedImage[] upSprites;
	private BufferedImage[] downBoatSprites;
	private BufferedImage[] leftBoatSprites;
	private BufferedImage[] rightBoatSprites;
	private BufferedImage[] upBoatSprites;
	private BufferedImage image;

	public final int screenX;
	public final int screenY;

	private int spriteCounter = 0;
	private int spriteNum = 1;
	// gameplay
	// private int numDiamonds;
	// private int totalDiamonds;
	// private boolean hasBoat;
	// private boolean hasAxe;
	// private boolean onWater;
	// private long ticks;

	GamePanel gp;

	public Player() {
		// numDiamonds = 0;

		downSprites = Content.PLAYER[0];
		leftSprites = Content.PLAYER[1];
		rightSprites = Content.PLAYER[2];
		upSprites = Content.PLAYER[3];
		downBoatSprites = Content.PLAYER[4];
		leftBoatSprites = Content.PLAYER[5];
		rightBoatSprites = Content.PLAYER[6];
		upBoatSprites = Content.PLAYER[7];

		screenX = gp.SCREEN_WIDTH / 2 - gp.TILE_SIZE / 2;
		screenY = gp.SCREEN_HEIGHT / 2 - gp.TILE_SIZE / 2;

		setDefaultValues();
	}

	public Player(GamePanel gp) {

		this.gp = gp;
		// numDiamonds = 0;

		downSprites = Content.PLAYER[0];
		leftSprites = Content.PLAYER[1];
		rightSprites = Content.PLAYER[2];
		upSprites = Content.PLAYER[3];
		downBoatSprites = Content.PLAYER[4];
		leftBoatSprites = Content.PLAYER[5];
		rightBoatSprites = Content.PLAYER[6];
		upBoatSprites = Content.PLAYER[7];

		screenX = gp.SCREEN_WIDTH / 2 - gp.TILE_SIZE / 2;
		screenY = gp.SCREEN_HEIGHT / 2 - gp.TILE_SIZE / 2;

		// set SolidArea
		solidArea = new Rectangle();
		solidArea.x = 8;
		solidArea.y = 8;
		solidArea.width = 32;
		solidArea.height = 32;
		setDefaultValues();

	}

	public void setDefaultValues() {
		worldX = gp.TILE_SIZE * 17;
		worldY = gp.TILE_SIZE * 17;
		speed = 2;
		image = downSprites[0];
		direction = "down";
	}

	public void update() {
		handleInput();
	}

	public void draw(Graphics2D g) {
		int x = screenX;
		int y = screenY;
//		System.out.println(worldX + " "+ worldY);	
		
		if (screenX > worldX) {
			x = worldX;
		}
		if (screenY > worldY) {
			y = worldY;
		}
		int rightOffset = gp.SCREEN_WIDTH - screenX;
		if (rightOffset > gp.WORLD_WIDTH - worldX) {
			x = gp.SCREEN_WIDTH - gp.WORLD_WIDTH + worldX;
		}
		int bottomOffset = gp.SCREEN_HEIGHT - screenY;
		if (bottomOffset > gp.WORLD_HEIGHT - worldY) {
			y = gp.SCREEN_HEIGHT - gp.WORLD_HEIGHT + worldY;
		}
		
		
		g.drawImage(image, x, y, gp.TILE_SIZE, gp.TILE_SIZE, null);

	}

	public void check() {
		collisionOn = false;
		gp.cChecker.checkTile(this);
		// IF COLLISION IS FALSE, PLAYER CAN MOVE
		if (collisionOn == false) {
			switch (direction) {
				case "up":
					worldY -= speed;
					break;
				case "down":
					worldY += speed;
					break;
				case "left":
					worldX -= speed;
					break;
				case "right":
					worldX += speed;
					break;
			}
		}
	}

	public void handleInput() {
		if (KeyHandler.isDown(KeyHandler.DOWN)) {
			if (spriteNum == 1) {
				image = downSprites[0];
			}
			if (spriteNum == 2) {
				image = downSprites[1];
			}
			direction = "down";
			check();
		}
		if (KeyHandler.isDown(KeyHandler.UP)) {
			if (spriteNum == 1) {
				image = upSprites[0];
			}
			if (spriteNum == 2) {
				image = upSprites[1];
			}
			direction = "up";
			check();
		}
		if (KeyHandler.isDown(KeyHandler.LEFT)) {
			if (spriteNum == 1) {
				image = leftSprites[0];
			}
			if (spriteNum == 2) {
				image = leftSprites[1];
			}
			direction = "left";
			check();
		}
		if (KeyHandler.isDown(KeyHandler.RIGHT)) {
			if (spriteNum == 1) {
				image = rightSprites[0];
			}
			if (spriteNum == 2) {
				image = rightSprites[1];
			}
			direction = "right";
			check();
		}

		spriteCounter++;
		if (spriteCounter > 16) {
			spriteCounter = 0;
			if (spriteNum == 1) {
				spriteNum = 2;
			} else if (spriteNum == 2) {
				spriteNum = 1;
			}
		}
	}
}
