package hedspi.group4.tilemap;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

import hedspi.group4.main.GamePanel;

public class TileManager {

	// map
	public int[][] map;
	private int tileSize;
	private int numRows;
	private int numCols;
	// tileset
	private BufferedImage tileset;
	public static int numTilesAcross;
	public Tile[][] tiles;

	// drawing
	private int rowOffset;
	private int colOffset;
	private int numRowsToDraw;
	private int numColsToDraw;

	GamePanel gp;

	public TileManager(GamePanel gp) {
		this.gp = gp;
		this.tileSize = 16;
		numRowsToDraw = GamePanel.SCREEN_HEIGHT / tileSize + 2;
		numColsToDraw = GamePanel.SCREEN_WIDTH / tileSize + 2;
		loadTiles("/Tilesets/testtileset.gif");
		loadMap("/Maps/testmap2.map");
//		speed = 4;
	}

	public void loadTiles(String s) {

		try {

			tileset = ImageIO.read(getClass().getResourceAsStream(s));
			numTilesAcross = tileset.getWidth() / tileSize;
			tiles = new Tile[2][numTilesAcross];

			BufferedImage subimage;
			for (int col = 0; col < numTilesAcross; col++) {
				subimage = tileset.getSubimage(col * tileSize, 0, tileSize, tileSize);
				tiles[0][col] = new Tile(subimage, Tile.NORMAL);
				subimage = tileset.getSubimage(col * tileSize, tileSize, tileSize, tileSize);
				tiles[1][col] = new Tile(subimage, Tile.BLOCKED);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void loadMap(String s) {

		try {

			InputStream in = getClass().getResourceAsStream(s);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			numCols = Integer.parseInt(br.readLine());
			numRows = Integer.parseInt(br.readLine());
			map = new int[numRows][numCols];
			String delims = "\\s+";
			for (int row = 0; row < numRows; row++) {
				String line = br.readLine();
				String[] tokens = line.split(delims);
				for (int col = 0; col < numCols; col++) {
					map[row][col] = Integer.parseInt(tokens[col]);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void draw(Graphics2D g) {

		for (int row = rowOffset; row < rowOffset + numRowsToDraw; row++) {

			if (row >= numRows)
				break;

			for (int col = colOffset; col < colOffset + numColsToDraw; col++) {

				if (col >= numCols)
					break;
				if (map[row][col] == 0)
					continue;
				
				int rc = map[row][col];
				int r = rc / numTilesAcross;
				int c = rc % numTilesAcross;

				int worldX = col * GamePanel.TILE_SIZE;
				int worldY = row * GamePanel.TILE_SIZE;
				int screenX = worldX - gp.player.worldX + gp.player.screenX;
				int screenY = worldY - gp.player.worldY + gp.player.screenY;

				if (gp.player.screenX > gp.player.worldX) {
					screenX = worldX;
				}
				if (gp.player.screenY > gp.player.worldY) {
					screenY = worldY;
				}
				int rightOffset = GamePanel.SCREEN_WIDTH - gp.player.screenX;
				if (rightOffset > GamePanel.WORLD_WIDTH - gp.player.worldX) {
					screenX = GamePanel.SCREEN_WIDTH - GamePanel.WORLD_WIDTH + worldX;
				}
				int bottomOffset = GamePanel.SCREEN_HEIGHT - gp.player.screenY;
				if (bottomOffset > GamePanel.WORLD_HEIGHT - gp.player.worldY) {
					screenY = GamePanel.SCREEN_HEIGHT - GamePanel.WORLD_HEIGHT + worldY;
				}

				if (worldX + GamePanel.TILE_SIZE > gp.player.worldX - gp.player.screenX
						&& worldX - GamePanel.TILE_SIZE < gp.player.worldX + gp.player.screenX
						&& worldY + GamePanel.TILE_SIZE > gp.player.worldY - gp.player.screenY
						&& worldY - GamePanel.TILE_SIZE < gp.player.worldY + gp.player.screenY) {

					g.drawImage(tiles[r][c].getImage(), screenX, screenY, GamePanel.TILE_SIZE, GamePanel.TILE_SIZE,
							null);

				} else if (gp.player.screenX > gp.player.worldX || gp.player.screenY > gp.player.worldY
						|| rightOffset > GamePanel.WORLD_WIDTH - gp.player.worldX
						|| bottomOffset > GamePanel.WORLD_HEIGHT - gp.player.worldY) {
					g.drawImage(tiles[r][c].getImage(), screenX, screenY, GamePanel.TILE_SIZE, GamePanel.TILE_SIZE,
							null);
				}

			}

		}

	}
}
