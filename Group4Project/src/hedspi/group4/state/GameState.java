// Blueprint for all GameState subclasses.
// Has a reference to the GameStateManager
// along with the four methods that must
// be overridden.

package hedspi.group4.state;

import java.awt.Graphics2D;

import hedspi.group4.main.GamePanel;
import hedspi.group4.manager.GameStateManager;

public abstract class GameState {

	protected GamePanel gp;
	
	public GameState(GamePanel gp) {
		this.gp = gp;
		
	}

	public abstract void init();

	public abstract void update();

	public abstract void draw(Graphics2D g);

	public abstract void handleInput();

}
