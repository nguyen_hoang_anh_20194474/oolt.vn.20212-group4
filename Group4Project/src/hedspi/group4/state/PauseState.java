//
//// by calling GameStateManager#setPaused(true).
//
//package hedspi.group4.state;
//
//import hedspi.group4.main.GamePanel;
//import hedspi.group4.manager.*;
//import java.awt.Graphics2D;
//
//public class PauseState extends GameState {
//
//	public PauseState( GameStateManager gsm) {
//		super( gsm);
//	}
//
//	public void init() {
//	}
//
//	public void update() {
//		handleInput();
//	}
//
//	public void draw(Graphics2D g) {
//
//		Content.drawString(g, "paused", 40, 30, 16);
//
//		Content.drawString(g, "arrow", 12, 76, 16);
//		Content.drawString(g, "keys", 16, 84, 16);
//		Content.drawString(g, ": move", 52, 80, 16);
//
//		Content.drawString(g, "space", 12, 96, 16);
//		Content.drawString(g, ": action", 52, 96, 16);
//
//		Content.drawString(g, "F1:", 36, 112, 16);
//		Content.drawString(g, "return", 68, 108, 16);
//		Content.drawString(g, "to menu", 68, 116, 16);
//
//	}
//
//	public void handleInput() {
//		if (KeyHandler.isPressed(KeyHandler.ESCAPE)) {
//			gsm.setPaused(false);
////			JukeBox.resumeLoop("music1");
//		}
//		if (KeyHandler.isPressed(KeyHandler.F1)) {
//			gsm.setPaused(false);
//			gsm.setState(GameStateManager.MENU);
//		}
//	}
//
//}
