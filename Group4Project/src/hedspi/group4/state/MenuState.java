// The main menu GameState.

package hedspi.group4.state;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import hedspi.group4.main.GamePanel;
import hedspi.group4.manager.*;

public class MenuState extends GameState {

	private BufferedImage bg;
	private BufferedImage diamond;

	private int currentOption = 0;
	private String[] options = { "START", "QUIT" };

	public MenuState(GamePanel gp) {
		super(gp);
	}

	public void init() {
		bg = Content.MENUBG[0][0];
		diamond = Content.DIAMOND[0][0];
//		JukeBox.load("/SFX/collect.wav", "collect");
//		JukeBox.load("/SFX/menuoption.wav", "menuoption");
	}

	public void update() {
		handleInput();
	}

	public void draw(Graphics2D g) {

		g.drawImage(bg, 0, 0, GamePanel.SCREEN_WIDTH, GamePanel.SCREEN_HEIGHT, null);

		Content.drawString(g, options[0], GamePanel.SCREEN_WIDTH / 3, GamePanel.SCREEN_HEIGHT / 2 + 100, 32);
		Content.drawString(g, options[1], GamePanel.SCREEN_WIDTH / 3, GamePanel.SCREEN_HEIGHT / 2 + 150, 32);

		if (currentOption == 0)
			g.drawImage(diamond, GamePanel.SCREEN_WIDTH / 3 - 35, GamePanel.SCREEN_HEIGHT / 2 + 100, 32, 32, null);
		else if (currentOption == 1)
			g.drawImage(diamond, GamePanel.SCREEN_WIDTH / 3 - 35, GamePanel.SCREEN_HEIGHT / 2 + 150, 32, 32, null);

	}

	public void handleInput() {
		if (KeyHandler.isPressed(KeyHandler.DOWN) && currentOption < options.length - 1) {
//			JukeBox.play("menuoption");
			currentOption++;
		}
		if (KeyHandler.isPressed(KeyHandler.UP) && currentOption > 0) {
//			JukeBox.play("menuoption");
			currentOption--;
		}
		if (KeyHandler.isPressed(KeyHandler.ENTER)) {
//			JukeBox.play("collect");
			selectOption();
		}
	}

	private void selectOption() {
		if (currentOption == 0) {
//			gsm.setState(GameStateManager.PLAY);
		}
		if (currentOption == 1) {
			System.exit(0);
		}
	}

}
